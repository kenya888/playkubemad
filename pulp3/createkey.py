from cryptography.fernet import Fernet
import os

certdir = 'certs'
if not os.path.exists(certdir):
  os.makedirs(certdir)

key = Fernet.generate_key()
file = open(certdir + '/database_fields.symmetric.key', 'w')
file.write(key.decode('utf-8'))
file.close()
