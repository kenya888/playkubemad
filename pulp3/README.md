# Pulp3 podman kube play yaml

[Pulp](https://pulpproject.org) is a platform for managing repositories of software packages and making them available to a large number of consumers.
These play-kube yaml files are for development/testing use only. If you want to setup pulp3 for production, consider using [another choice](https://pulpproject.org/)

## Instruction

### Prerequisites

- [Podman](https://podman.io/getting-started/installation) v4.3.0 or later on your favorite Linux distro
- [pulp-cli](https://docs.pulpproject.org/pulp_cli/install/)
- [python-cryptography](https://cryptography.io/en/latest/)
- git

### Installation

Prerequisites tools installation example (Fedora 36 or later)
```bash
$ sudo dnf install -y podman python3-pip python3-cryptography
$ pip install --user pulp-cli
```

Clone the repository
```bash
$ git clone https://gitlab.com/kenya888/playkubemad
$ cd playkubemad/pulp3
```

Run createkey.py
```bash
$ python3 createkey.py
```
You can find the pulp db encryption file in `certs/database_fields.symmetric.key`

Run podman kube play
```bash
$ podman kube play --configmap pulp-configmap.yml,pulp-nginx-configmap.yml,pulp-settings-configmap.yml pod.yml
```

Check all pod and containers are running
```bash
$ podman pod ps
POD ID        NAME        STATUS      CREATED         INFRA ID      # OF CONTAINERS
21ab54c6b47c  pulp        Running     29 minutes ago  225a95bb657c  10

$ podman ps
CONTAINER ID  IMAGE                                    COMMAND               CREATED         STATUS         PORTS                                                         NAMES
225a95bb657c  localhost/podman-pause:4.8.1-1701859098                        28 minutes ago  Up 27 minutes  0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  21ab54c6b47c-infra
c755c6f5ead9  docker.io/library/postgres:16            postgres              28 minutes ago  Up 27 minutes  0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-postgres
abbad05c511c  docker.io/library/redis:latest           redis-server          27 minutes ago  Up 27 minutes  0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-redis
f9ce5d1fff55  quay.io/pulp/pulp-web:latest             /bin/sh -c nginx ...  27 minutes ago  Up 27 minutes  0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-web
f3647f867e98  quay.io/pulp/pulp-minimal:latest                               27 minutes ago  Up 27 minutes  0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-api
abb55de4487d  quay.io/pulp/pulp-minimal:latest                               27 minutes ago  Up 27 minutes  0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-content
563ac86ef9d9  quay.io/pulp/pulp-minimal:latest                               27 minutes ago  Up 27 minutes  0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-worker
f7731db8509d  quay.io/pulp/pulp-minimal:latest                               27 minutes ago  Up 3 seconds   0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-migration_service
3a38fc4511f3  quay.io/pulp/pulp-minimal:latest                               27 minutes ago  Up 2 seconds   0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-signing_key_service
c623d456bd98  quay.io/pulp/pulp-minimal:latest                               27 minutes ago  Up 11 seconds  0.0.0.0:8080->8080/tcp, 0.0.0.0:24816-24817->24816-24817/tcp  pulp-set_init_password_service
```

### Test

The setup instruction of pulp-cli is [here](https://docs.pulpproject.org/pulp_cli/configuration/)

```bash
$ pulp config create -i
Config file location [/home/cloud-user/.config/pulp/cli.toml]:
API base url [https://localhost]: http://localhost:8080
Absolute API base path on server (not including 'api/v3/') [/pulp/]:
Username on pulp server []: admin
Password on pulp server []: password
Path to client certificate []:
Path to client private key. Not required if client cert contains this. []:
Verify SSL connection [True]: False
Format of the response (json, yaml, none) [json]:
Trace commands without performing any unsafe HTTP calls [False]:
Time to wait for background tasks, set to 0 to wait infinitely [0]:
Increase verbosity; explain api calls as they are made [0]:
Domain to work in if feature is enabled [default]:
Created config file at '/home/cloud-user/.config/pulp/cli.toml'
```

Test connection to pulp with pulp-cli

```bash
$ pulp user list
[
  {
    "pulp_href": "/pulp/api/v3/users/1/",
    "id": 1,
    "username": "admin",
    "first_name": "",
    "last_name": "",
    "email": "",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2024-01-05T09:12:48.589007Z",
    "groups": [],
    "hidden_fields": [
      {
        "name": "password",
        "is_set": true
      }
    ]
  }
]

```
Here is a good [instruction](https://docs.pulpproject.org/pulp_rpm/workflows/index.html) of pulp-cli rpm plugin to test if pulp is working fine.

### Stop and destroy Pod

```bash
$ podman pod stop pulp
$ podman pod rm pulp
$ podman volume rm pulp-nginx-config pulp-settings  #leave pulp content, redis and postgresql persistent volumes
```

If you want to delete pulp3 deployment completely, remove rest volumes.
```bash
$ podman volume rm pulp-content-data pulp-pg-data pulp-redis-data
```

## Known Issue

### Secret

The current podman implementation of secret [is not compatible](https://github.com/containers/podman/issues/10105) with the one of k8s.
So we cannot use secret with volume mount.
